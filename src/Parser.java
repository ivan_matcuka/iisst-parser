import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.*;

public class Parser {
    private Document document = null;
    private ArrayList<Post> posts = new ArrayList<>();

    void run() {
        try {
            document = Jsoup.connect("https://www.rsuh.ru/iintb/news/general/").get();
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.getNews();
    }

    public Document getDocument() {
        return document;
    }

    public ArrayList<Post> getPosts() {
        return this.posts;
    }

    /**
     * Populates post array.
     */
    private void getNews() {
        Elements news = this.document.select(".news_item");

        for (Element post : news) {
            String title = post.select(".news_item_headline").first().text();

            Element body = this.clearAttributes(
                    post.select(".news_body").first()
            );

            Post newPost = new Post(title, body.toString());

            this.posts.add(newPost);
        }
    }

    /**
     * Clears top-level attributes.
     *
     * @param element — Element object.
     * @return Element
     */
    private Element clearAttributes(Element element) {
        element.removeAttr("style");
        element.removeAttr("class");

        this.clearChildAttributes(element.childNodes());

        return element;
    }

    /**
     * Clears child attributes.
     *
     * @param nodes — Child nodes.
     */
    private void clearChildAttributes(List<Node> nodes) {
        for (Node node : nodes) {
            node.removeAttr("style");
            node.removeAttr("class");

            if (node.childNodes().size() > 0) {
                this.clearChildAttributes(node.childNodes());
            }
        }

    }
}
