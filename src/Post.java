import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

class Post {
    private String title;
    private String body;

    /**
     * Constructor.
     *
     * @param title — Title string
     * @param body  — Body HTML string.
     */
    Post(String title, String body) {
        this.title = title;
        this.body = body;
    }

    /**
     * Return the post as JSON.
     *
     * @return String
     */
    String toJSON() {
        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        return gson.toJson(this);
    }

    /**
     * Returns the title.
     *
     * @return String
     */
    public String getTitle() {
        return title;
    }

    /**
     * Returns the body.
     *
     * @return String
     */
    public String getBody() {
        return body;
    }
}
